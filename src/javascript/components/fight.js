import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const maxFirstFighterHealth = firstFighter.health;
    const maxSecondFighterHealth = secondFighter.health;
    let command = new Set();
    let timerForFirst;
    let firstX = 0;
    let timerForSecond;
    let secondX = 0;

    document.addEventListener('keyup', function (event) {
      command.delete(event.code);
    })

    document.addEventListener('keydown', function (event) {
      command.add(event.code);
      if (command.has(controls.PlayerOneAttack)) {
        if (!command.has(controls.PlayerOneBlock)) {
          let damage = 0;
          (command.has(controls.PlayerTwoBlock)) ? damage = getDamage(firstFighter, secondFighter): damage = getHitPower(firstFighter);
          secondFighter.health -= damage;
          let newWidth = secondFighter.health*100/maxSecondFighterHealth;
          let div = document.getElementById('right-fighter-indicator');
          div.style.width = newWidth + '%';
        }
      } 
      else if (command.has(controls.PlayerTwoAttack)) {
        if (!command.has(controls.PlayerTwoBlock)) {
          let damage = 0;
          (command.has(controls.PlayerOneBlock)) ? damage = getDamage(secondFighter, firstFighter): damage = getHitPower(secondFighter);
          firstFighter.health -= damage;
          let newWidth = firstFighter.health*100/maxFirstFighterHealth;
          let div = document.getElementById('left-fighter-indicator');
          div.style.width = newWidth + '%';
        }
      } 
      else if (controls.PlayerOneCriticalHitCombination.every(key => command.has(key))) {
        if (firstX <= 0) {
          let damage = firstFighter.attack * 2;
          secondFighter.health -= damage;
          let newWidth = secondFighter.health*100/maxSecondFighterHealth;
          let div = document.getElementById('right-fighter-indicator');
          div.style.width = newWidth + '%';
          firstX = 10;
        }
        coolDownForFirstFighter()

        function coolDownForFirstFighter() {
          firstX--;
          if (firstX < 0) {
            clearTimeout(timerForFirst);
          } else {
            timerForFirst = setTimeout(coolDownForFirstFighter, 1000);
          }
        }
      } 
      else if (controls.PlayerTwoCriticalHitCombination.every(key => command.has(key))) {
        if (secondX <= 0) {
          let damage = secondFighter.attack * 2;
          firstFighter.health -= damage;
          let newWidth = firstFighter.health*100/maxFirstFighterHealth;
          let div = document.getElementById('left-fighter-indicator');
          div.style.width = newWidth + '%';
          secondX = 10;
        }
        coolDownForSecondFighter()

        function coolDownForSecondFighter() {
          secondX--;
          if (secondX < 0) {
            clearTimeout(timerForSecond);
          } else {
            timerForSecond = setTimeout(coolDownForSecondFighter, 1000);
          }
        }
      }
      if (firstFighter.health <= 0) resolve(secondFighter)
      else if (secondFighter.health <= 0) resolve(firstFighter);
    });
  });
}


export function getDamage(attacker, defender) {
  let damage = getHitPower(attacker) - getBlockPower(defender);
  if(damage < 0) damage = 0;
  return damage
}

export function getHitPower(fighter) {
  let criticalHitChance = Math.random(1, 2);
  let power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  let dodgeChance = Math.random(1, 2);
  let power = fighter.defense * dodgeChance;
  return power;
}
import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  if(!fighter)
  {
    return '';
  }
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  let fighterElements = [];
  Object.keys(fighterToFighterPreviewInfo(fighter)).forEach((key) => { 
    (key==='source') ? fighterElements.push(createFighterImage(fighter)) : fighterElements.push(createParam(key, fighter[key]))
  });
  fighterElement.append(...fighterElements);
  return fighterElement;
}

function fighterToFighterPreviewInfo({name, health, attack, defense, source}) {
  return { 
     source,
     name,
     health,
     attack, 
     defense
  }
}

function createParam(paramName, param) {
  const paramElement = createElement({ tagName: 'span', className: 'fighter-preview___info' });
  paramElement.innerText = `${paramName} : ${param}`;

  return paramElement;
}
export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
